enum State {
  BOMB,
  EMPTY
}

PFont f;
PImage flag,bomb,empty;


// Requires cols = rows
// If cols != rows then in size() parameters must be changed.
final int cols = 20;
final int rows = 20;
final int bombPer = 10;
Grid grid;

void setup(){
  // The size must be shaping a square
  size(900,900);
  frameRate(30);
  noLoop();
  grid = new Grid(rows, cols, bombPer);
  
  f = createFont("Yrsa", 0.85f*(height/rows));// 0.85 is conversion factor
  textFont(f);
  
  flag = loadImage("flag2.png");
  flag.resize(height/rows-2,height/rows-2);
  bomb = loadImage("bomb.png");
  bomb.resize(height/rows-4,height/rows-4);
  empty = loadImage("empty.png");
  empty.resize(height/rows-2,height/rows-2);
}

void draw(){
  grid.drawGrid();
}

void mousePressed(){
  if(!grid.isGameOver){
    if(mouseButton == LEFT)
      grid.flip();
    else if(mouseButton == RIGHT)
      grid.flag();
  } else {
    grid = new Grid(rows, cols, bombPer);
  }
  redraw();
}
