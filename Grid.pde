public class Grid {
  int cols, rows;
  int emptySpaces;
  int visibleCells;
  boolean isGameOver;
  Cell[][] grid;
  
  Grid(int r, int c, int minPer){
    cols = c;
    rows = r;
    emptySpaces = r*c;
    visibleCells = 0;
    grid = new Cell[rows][cols];
    isGameOver= false;
    
    for(int i = 0;i < rows;i++){
      for(int j = 0;j < cols;j++){
        boolean isBomb = false;
        if(random(100) < minPer){
          isBomb = true;
          emptySpaces--;
        }
        grid[i][j] = new Cell(isBomb);
      }
    }
    
    for(int i = 0;i < rows;i++){
      for(int j = 0;j < cols;j++){
        grid[i][j].setNeighbors(countNeighbors(j,i));
      }
    }
  }
  
  void drawGrid(){
    background(255);
    stroke(125);
    
    for(int i = 0;i < rows;i++){
      line(0, i*height/rows, width, i*height/rows);
      line(i*width/cols, 0, i*width/cols , height);
    }
   
    
    for(int i = 0;i < rows;i++){
      for(int j = 0;j < cols;j++){
        if(grid[i][j].visible){
          fill(255);
          square(j*(width/cols), i*(height/rows), height/rows);
          if(grid[i][j].state == State.BOMB){
            // Draw bombs
            //fill(150);
            //circle((width/cols)*(0.5+j), (height/rows)*(0.5+i), (height/rows)/2);
            image(bomb,j*(width/cols)+3,i*(height/rows)+3);
          } else {
            if(grid[i][j].neighbors != 0){
              // If it has neighbors, then print number of neighbors
              fill(0);
              text(intToChar(grid[i][j].neighbors), (width/cols)*(0.35f+j), (height/rows)*(0.75f+i));
            }
          }
        } else {
          fill(150);
          square(j*(width/cols),i*(height/rows),height/rows);
          if(grid[i][j].flagged){
            image(flag,j*(width/cols)+1,i*(height/rows)+1);
          } else {
            image(empty,j*(width/cols)+1,i*(height/rows)+1);
          }
        }
      }
    }
  }
  
  void flip(){
    if(grid[mousePosY()][mousePosX()].state == State.BOMB){
      println("You lost!");
      gameOver();
    } else if(grid[mousePosY()][mousePosX()].visible) return;
    
    show(mousePosX(),mousePosY());
    
    if(visibleCells == emptySpaces){
      println("You WON!");
      gameOver();
    }
  }
  
  void flag(){
    if(grid[mousePosY()][mousePosX()].visible) return;
    grid[mousePosY()][mousePosX()].flagged = !(grid[mousePosY()][mousePosX()].flagged);
  }
  
  int countNeighbors(int x, int y){
    // Returns amount of bombs that surrounds grid[y][x]
    // Requires grid[y][x] to be empty
    int neighbors = 0;
    
    for(int i = -1;i < 2;i++){
      for(int j = -1;j < 2;j++){
        if( x+j >= 0 && x+j < cols && y+i >= 0 && y+i < rows && 
            grid[y+i][x+j].state == State.BOMB){
          neighbors++;
        }
      }
    }
    return neighbors;
  }
  
  int mousePosX(){ return mouseX/(width/cols);} // Returns x index where mouse was pressed
  int mousePosY(){ return mouseY/(height/rows);}// Returns y index where mouse was pressed
  
  char intToChar(int n){
    // Requires 0 <= n < 10
    char c = '0';
    switch(n){
      case 1: c = '1';break;
      case 2: c = '2';break;
      case 3: c = '3';break;
      case 4: c = '4';break;
      case 5: c = '5';break;
      case 6: c = '6';break;
      case 7: c = '7';break;
      case 8: c = '8';break;
      case 9: c = '9';break;
    }
    return c;
  }
  
  void show(int x, int y){
    // Requires the position grid[y][x] to be empty
    grid[y][x].visible = true; visibleCells++;
    grid[y][x].flagged = false;
    if(grid[y][x].neighbors == 0){
      for(int i = -1;i < 2;i++){
        for(int j = -1;j < 2;j++){
          if( x+j >= 0 && x+j < cols && y+i >= 0 && y+i < rows && // Check if coords grid[y+i][x+j] are within bounds
              grid[y+i][x+j].state == State.EMPTY && 
              !(grid[y+i][x+j].visible) ){ // Checks if the function didn't execute show() with grid[y+i][x+j] before
            show(x+j,y+i);
          }
        }
      }
    }
  }
  
  void gameOver(){
    isGameOver = true;
    for(int i = 0;i < rows;i++){
      for(int j = 0;j < cols;j++){
        grid[i][j].visible = true;
      }
    }
    drawGrid();
  }
}
