public class Cell {
  boolean visible;
  boolean flagged;
  int neighbors;
  State state;
  
  Cell(boolean isBomb){
    visible = false;
    flagged = false;
    
    if(isBomb){
      state = State.BOMB;
    } else {
      state = State.EMPTY;
    }
  }
  void setNeighbors(int n){
    neighbors = n;
  }
}
